# ContractorTest

Цель 
Создать RESTful API на Laravel для выполнения CRUD операций с сущностью Contractor 
(Контрагент). 
Требования 
1. Реализовать CRUD операции для сущности Contractor: 
a. Получение списка Контрагентов 
b. Получение одного Контрагента 
c. Создание Контрагента 
d. Обновление информации о Контрагенте 
e. Удаление Контрагента 
2. API должно быть RESTful и иметь соответствующие маршруты для каждого действия. 
3. Реализовать валидацию данных перед выполнением операций. 
Структура базы данных 
Contractor: 
 Название (string) 
 ИНН (должна быть валидация на количество символов 10) 
 Email (должна быть валидация на соответствие) 
Методы API 
Маршруты: 
 POST /api/contractor - создание нового Контрагента 
 GET /api/contractor – получение списка Контрагентов 
 GET /api/contractor/{id} – получение данных Контрагента 
 PUT /api/contractor/{id} - обновление информации об Контрагенте 
 DELETE /api/contractor/{id} - удаление Контрагента

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/linamilklina/contractortest.git
git branch -M main
git push -uf origin main
```
