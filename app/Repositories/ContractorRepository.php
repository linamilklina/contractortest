<?php

namespace App\Repositories;

use App\Models\Contractor;

class ContractorRepository
{
    public function create(array $attributes)
    {
        return Contractor::create($attributes);
    }

    public function getAll()
    {
        return Contractor::all();
    }

    public function getById($id)
    {
        return Contractor::find($id);
    }

    public function update($id, array $attributes)
    {
        $contractor = $this->getById($id);
        if ($contractor) {
            $contractor->update($attributes);
            return $contractor;
        }
        return null;
    }

    public function delete($id)
    {
        $contractor = $this->getById($id);
        if ($contractor) {
            return $contractor->delete();
        }
        return false;
    }
}
