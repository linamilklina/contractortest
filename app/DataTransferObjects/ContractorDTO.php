<?php

namespace App\DataTransferObjects;

use App\ValueObjects\EmailValueObject;

class ContractorDTO
{
    private $name;
    private $tin;
    private $email;

    public function __construct($name, $tin, $email)
    {
        $this->name = $name;
        $this->tin = $tin;
        $this->email = $email;
    }

    public function getName()
    {
        // test test
        // test test test 
        return $this->name;
        // test
        // это должно оказаться в test
    }

    public function getTin()
    {
        return $this->tin;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function toArray()
    {
        return [
            'name' => $this->name,
            'TIN' => $this->tin,
            'email' => $this->email,
        ];
    }

    public static function createFromRequest($validated)
    {
        $email = new EmailValueObject($validated['email']);
        return new self(
            $validated['name'],
            $validated['TIN'],
            $email->getEmail(),
        );
    }
}
