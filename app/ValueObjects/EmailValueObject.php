<?php

namespace App\ValueObjects;

class EmailValueObject
{
    private string $email;

    public function __construct(string $email)
    {
        if (!preg_match('/^\S+@\S+\.\S+$/', $email)) {
            throw new \InvalidArgumentException('Некорректный email');
        }
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }
}
