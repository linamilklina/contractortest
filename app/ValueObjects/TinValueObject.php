<?php

namespace App\ValueObjects;

class TinValueObject
{
    private string $inn;

    public function __construct(string $inn)
    {
        if (!preg_match('/^\d{10}$/', $inn)) {
            throw new \InvalidArgumentException('ИНН должен содержать 10 цифр.');
        }
        $this->inn = $inn;
    }

    public function getInn()
    {
        return $this->inn;
    }
}
