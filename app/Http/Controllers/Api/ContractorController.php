<?php

namespace App\Http\Controllers\Api;

use App\Repositories\ContractorRepository;
use App\DataTransferObjects\ContractorDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateContractorRequest;
use App\Http\Requests\UpdateContractorRequest;

class ContractorController extends Controller
{
    protected $contractorRepository;

    public function __construct(ContractorRepository $contractorRepository)
    {
        $this->contractorRepository = $contractorRepository;
    }

    public function index()
    {
        $contractors = $this->contractorRepository->getAll();
        return response()->json($contractors);
    }

    public function store(CreateContractorRequest $request)
    {
        $validated = $request->validated();
        $contractorDTO = ContractorDTO::createFromRequest($validated);
        $contractor = $this->contractorRepository->create($contractorDTO->toArray());
        return response()->json($contractor);
    }

    public function show($id)
    {
        $contractor = $this->contractorRepository->getById($id);
        return response()->json($contractor);
    }

    public function update(UpdateContractorRequest $request, $id)
    {
        $validated = $request->validated();
        $contractorDTO = ContractorDTO::createFromRequest($validated);
        $contractor = $this->contractorRepository->update($id, $contractorDTO->toArray());
        return response()->json($contractor);
    }

    public function destroy($id)
    {
        $deleted = $this->contractorRepository->delete($id);
        return response()->json(['deleted' => $deleted]);
    }
}
