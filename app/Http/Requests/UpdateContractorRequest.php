<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;

class UpdateContractorRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'TIN' => 'required|string|size:10',
            'email' => 'required|email',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors();

        $response = response()->json([
            'errors' => $errors
        ], 422);

        throw new ValidationException($validator, $response);
    }

    public function messages()
    {
        return [
            'name.required' => 'Поле "Имя" обязательно для заполнения.',
            'name.string' => 'Поле "Имя" должно быть строкой.',
            'TIN.required' => 'Поле "ИНН" обязательно для заполнения.',
            'TIN.string' => 'Поле "ИНН" должно быть строкой.',
            'TIN.size' => 'Поле "ИНН" должно содержать ровно 10 символов.',
            'email.required' => 'Поле "Электронная почта" обязательно для заполнения.',
            'email.email' => 'Поле "Электронная почта" должно содержать действительный электронный адрес.',
        ];
    }
}
