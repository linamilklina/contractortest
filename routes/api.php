<?php

use App\Http\Controllers\Api\ContractorController;
use Illuminate\Support\Facades\Route;

Route::apiResource('contractor', ContractorController::class);
